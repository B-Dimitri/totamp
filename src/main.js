import Vue from 'vue';
import App from './App.vue';
import './style/scss/init.scss';
import './registerServiceWorker';

Vue.config.productionTip = false

new Vue({
  render: function (h) { return h(App) },
}).$mount('#app')
